<?php
class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    // un getter
    public function getNom(): string {
        return $this->nom;
    }

    // un setter
    public function setNom(string $nom) {
        $this->nom = $nom;
    }

    // un constructeur
    public function __construct(
        string $login,
        string $nom,
        string $prenom
    )
    {
        $this->login = $login.$this->setLogin($login);
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin() : string
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */

    /**
     * @return mixed
     */
    public function getPrenom() : string
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom(string $prenom)
    {
        $this->prenom = $prenom;
    }

    public function setLogin(string $login) {
        $this->login = substr($login,0,64);
    }

    // Pour pouvoir convertir un objet en chaîne de caractères
    public function __toString() : string {
        return "Utilisateur $this->prenom $this->nom de login $this->login";
    }

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau) : Utilisateur {
        return new Utilisateur($utilisateurFormatTableau['loginBaseDeDonnees'],$utilisateurFormatTableau['nomBaseDeDonnees'],$utilisateurFormatTableau['prenomBaseDeDonnees']);
    }

    public static function recupererUtilisateurs(){
        $pdo = ConnexionBaseDeDonnees::getPdo();
        $requete = "SELECT * FROM utilisateur";
        $pdoStatement = $pdo->query($requete);
        foreach ($pdoStatement as $utilisateurFormatTableau) {
            $tableau[] = Utilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $tableau;
    }
}
?>